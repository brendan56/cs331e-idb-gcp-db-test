# beginning of main3.py
from flask import render_template, jsonify
from create_db import app, db, Book, create_books
import subprocess

#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]

@app.route('/')
def index():
	return render_template('hello.html')
	
@app.route('/books/')
def book():
	books = db.session.query(Book).all()
	return render_template('books.html', books = books)

@app.route('/books/json/')
def bookjson():
	books = db.session.query(Book).all()
	return jsonify(Books=[e.serialize() for e in books])

@app.route('/test/')
def test():
	p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output=err+out
	output = output.decode("utf-8") #convert from byte type to string type
	
	return render_template('test.html', output = "<br/>".join(output.split("\n")))

	
if __name__ == "__main__":
	app.run()
# end of main3.py
